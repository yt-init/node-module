#!/usr/bin/env node

import { fileURLToPath } from 'node:url'
import { basename, dirname, resolve } from 'node:path'
import { execSync } from 'node:child_process'

import commandLineArgs from 'command-line-args'

import { copyFiles } from './copy-files'
import { mayReadPackageJson, writePackageJson } from './fs-util'
import { yarnAdd } from './yarn-add'

const main = async () => {
  const options = commandLineArgs([{ name: 'dir', defaultOption: true }])
  const { dir } = options
  if ('string' !== typeof dir || !dir) throw new Error('dir is required')
  const directory = resolve(process.cwd(), dir)
  console.log(`${directory} を初期化します`)
  const __dirname = dirname(fileURLToPath(import.meta.url))
  await copyFiles({ dist: directory, src: resolve(__dirname, '..') })
  await execSync('volta pin node@lts yarn@4', { cwd: directory })
  const {
    volta,
    packageManager: pmOrig,
    ...packageJsonCont
  } = (await mayReadPackageJson(directory)) || {}
  const packageManager =
    pmOrig ||
    (volta &&
      'object' === typeof volta &&
      'yarn' in volta &&
      'string' === typeof volta.yarn &&
      /^[0-9]+\.[0-9]+\.[0-9]+$/.test(volta.yarn) &&
      `yarn@${volta.yarn}`)
  await writePackageJson(directory, {
    name: basename(directory),
    version: '0.0.1',
    description: '',
    author: { name: 'ytoune' },
    engines: { node: '>= 20' },
    ...(volta ? { volta } : {}),
    ...(packageManager ? { packageManager } : {}),
    license: 'MIT',
    private: true,
    scripts: {
      test: 'eslint "**/*.{ts,tsx}" && tsc --noEmit && vitest run --passWithNoTests',
      'ts-node': 'node -r esbuild-register',
    },
    dependencies: {},
    devDependencies: {},
    prettier: {
      useTabs: false,
      singleQuote: true,
      trailingComma: 'all',
      semi: false,
      arrowParens: 'avoid',
      htmlWhitespaceSensitivity: 'ignore',
    },
    ...(packageJsonCont || {}),
  })
  await yarnAdd({
    directory,
    devs: [
      '@types/node',
      '@typescript-eslint/eslint-plugin',
      '@typescript-eslint/parser',
      'esbuild',
      'esbuild-register',
      'eslint',
      'eslint-config-prettier',
      'eslint-plugin-import',
      'eslint-plugin-github',
      'eslint-import-resolver-typescript',
      'prettier',
      'rimraf',
      'typescript',
      'vite',
      'vitest',
      '@eslint/compat',
      'globals',
      '@eslint/js',
      '@eslint/eslintrc',
    ],
  })
}

main()
  .then(() => {
    console.log('Done!!')
  })
  .catch(x => {
    console.error(x)
    process.exit(1)
  })
