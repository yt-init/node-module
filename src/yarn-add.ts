import { parseNi, run } from '@antfu/ni'

export interface Props {
  readonly names?: readonly string[]
  readonly devs?: readonly string[]
  readonly opts?: readonly string[]
  readonly directory: string
}

export const yarnAdd = async ({ names, devs, directory, opts = [] }: Props) => {
  if (names?.length)
    await run(parseNi, ['-ED', ...opts, ...names], { cwd: directory })
  if (devs?.length)
    await run(parseNi, ['-ED', ...opts, ...devs], { cwd: directory })
}
