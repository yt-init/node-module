export const ignoreNoEntry = (x: unknown) =>
  'ENOENT' !== (x as { code?: string } | null)?.code && Promise.reject(x)

export const ignoreEntryExist = (x: unknown) =>
  'EEXIST' !== (x as { code?: string } | null)?.code && Promise.reject(x)
