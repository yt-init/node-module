import { constants } from 'node:fs'
import {
  copyFile,
  mkdir,
  readdir,
  readFile,
  stat,
  writeFile,
} from 'node:fs/promises'
import { join } from 'node:path'

import { ignoreEntryExist, ignoreNoEntry } from './ignore-no-entry'

const readJson = async (path: string) => {
  const r = await readFile(path, 'utf8')
  return JSON.parse(r) as unknown
}

type JSON = Record<string, unknown>
export const writePackageJson = async (directory: string, data: JSON) => {
  const s = JSON.stringify(data, null, 2)
  await writeFile(join(directory, 'package.json'), s, 'utf8')
}

export const mayReadPackageJson = (directory: string) =>
  readJson(join(directory, 'package.json')).catch(ignoreNoEntry) as Promise<
    JSON | false
  >

export const copy = async (src: string, dst: string) => {
  const s = await stat(src)
  try {
    if (s.isDirectory()) {
      await mkdir(dst, { recursive: true })
      for (const name of await readdir(src)) {
        await copy(join(src, name), join(dst, name))
      }
    } else if (s.isFile()) {
      await copyFile(src, dst, constants.COPYFILE_EXCL)
    }
  } catch (x) {
    return await ignoreEntryExist(x)
  }
}
