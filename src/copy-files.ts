import { mkdir, stat } from 'node:fs/promises'
import { basename, join } from 'node:path'

import { copy } from './fs-util'

const reject = (x: unknown) => {
  throw x
}

export interface Props {
  readonly dist: string
  readonly src: string

  readonly files?: readonly (string | readonly [string, string])[]
}

export const copyFiles = async ({ dist, src, files: _files = [] }: Props) => {
  await stat(dist).then(
    s =>
      s.isDirectory() ||
      reject(new Error(`${basename(dist)} is not directory`)),
    async x => {
      if ('ENOENT' === (x as { code: string } | null)?.code)
        await mkdir(dist, { recursive: true })
      else reject(x)
    },
  )
  const files: readonly (string | readonly [string, string])[] = [
    '.editorconfig',
    'eslint.config.mjs',
    ['_gitignore', '.gitignore'],
    '.vscode',
    // 'scripts',
    'tsconfig.json',
    '.yarnrc.yml',
    ..._files,
  ]
  await Promise.all(
    files.map(async file => {
      const [source, target] = 'string' === typeof file ? [file, file] : file
      await copy(join(src, source), join(dist, target))
    }),
  )
}
