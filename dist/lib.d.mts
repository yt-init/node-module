interface Props$1 {
    readonly dist: string;
    readonly src: string;
    readonly files?: readonly (string | readonly [string, string])[];
}
declare const copyFiles: ({ dist, src, files: _files }: Props$1) => Promise<void>;

interface Props {
    readonly names?: readonly string[];
    readonly devs?: readonly string[];
    readonly opts?: readonly string[];
    readonly directory: string;
}
declare const yarnAdd: ({ names, devs, directory, opts }: Props) => Promise<void>;

declare const ignoreNoEntry: (x: unknown) => false | Promise<never>;
declare const ignoreEntryExist: (x: unknown) => false | Promise<never>;

export { copyFiles, ignoreEntryExist, ignoreNoEntry, yarnAdd };
