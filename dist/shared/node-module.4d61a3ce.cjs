'use strict';

const promises = require('node:fs/promises');
const node_path = require('node:path');
const node_fs = require('node:fs');
const ni = require('@antfu/ni');

const ignoreNoEntry = (x) => "ENOENT" !== x?.code && Promise.reject(x);
const ignoreEntryExist = (x) => "EEXIST" !== x?.code && Promise.reject(x);

const readJson = async (path) => {
  const r = await promises.readFile(path, "utf8");
  return JSON.parse(r);
};
const writePackageJson = async (directory, data) => {
  const s = JSON.stringify(data, null, 2);
  await promises.writeFile(node_path.join(directory, "package.json"), s, "utf8");
};
const mayReadPackageJson = (directory) => readJson(node_path.join(directory, "package.json")).catch(ignoreNoEntry);
const copy = async (src, dst) => {
  const s = await promises.stat(src);
  try {
    if (s.isDirectory()) {
      await promises.mkdir(dst, { recursive: true });
      for (const name of await promises.readdir(src)) {
        await copy(node_path.join(src, name), node_path.join(dst, name));
      }
    } else if (s.isFile()) {
      await promises.copyFile(src, dst, node_fs.constants.COPYFILE_EXCL);
    }
  } catch (x) {
    return await ignoreEntryExist(x);
  }
};

const reject = (x) => {
  throw x;
};
const copyFiles = async ({ dist, src, files: _files = [] }) => {
  await promises.stat(dist).then(
    (s) => s.isDirectory() || reject(new Error(`${node_path.basename(dist)} is not directory`)),
    async (x) => {
      if ("ENOENT" === x?.code)
        await promises.mkdir(dist, { recursive: true });
      else
        reject(x);
    }
  );
  const files = [
    ".editorconfig",
    "eslint.config.mjs",
    ["_gitignore", ".gitignore"],
    ".vscode",
    // 'scripts',
    "tsconfig.json",
    ".yarnrc.yml",
    ..._files
  ];
  await Promise.all(
    files.map(async (file) => {
      const [source, target] = "string" === typeof file ? [file, file] : file;
      await copy(node_path.join(src, source), node_path.join(dist, target));
    })
  );
};

const yarnAdd = async ({ names, devs, directory, opts = [] }) => {
  if (names?.length)
    await ni.run(ni.parseNi, ["-ED", ...opts, ...names], { cwd: directory });
  if (devs?.length)
    await ni.run(ni.parseNi, ["-ED", ...opts, ...devs], { cwd: directory });
};

exports.copyFiles = copyFiles;
exports.ignoreEntryExist = ignoreEntryExist;
exports.ignoreNoEntry = ignoreNoEntry;
exports.mayReadPackageJson = mayReadPackageJson;
exports.writePackageJson = writePackageJson;
exports.yarnAdd = yarnAdd;
