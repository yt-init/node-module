import { writeFile, stat, mkdir, readdir, copyFile, readFile } from 'node:fs/promises';
import { join, basename } from 'node:path';
import { constants } from 'node:fs';
import { run, parseNi } from '@antfu/ni';

const ignoreNoEntry = (x) => "ENOENT" !== x?.code && Promise.reject(x);
const ignoreEntryExist = (x) => "EEXIST" !== x?.code && Promise.reject(x);

const readJson = async (path) => {
  const r = await readFile(path, "utf8");
  return JSON.parse(r);
};
const writePackageJson = async (directory, data) => {
  const s = JSON.stringify(data, null, 2);
  await writeFile(join(directory, "package.json"), s, "utf8");
};
const mayReadPackageJson = (directory) => readJson(join(directory, "package.json")).catch(ignoreNoEntry);
const copy = async (src, dst) => {
  const s = await stat(src);
  try {
    if (s.isDirectory()) {
      await mkdir(dst, { recursive: true });
      for (const name of await readdir(src)) {
        await copy(join(src, name), join(dst, name));
      }
    } else if (s.isFile()) {
      await copyFile(src, dst, constants.COPYFILE_EXCL);
    }
  } catch (x) {
    return await ignoreEntryExist(x);
  }
};

const reject = (x) => {
  throw x;
};
const copyFiles = async ({ dist, src, files: _files = [] }) => {
  await stat(dist).then(
    (s) => s.isDirectory() || reject(new Error(`${basename(dist)} is not directory`)),
    async (x) => {
      if ("ENOENT" === x?.code)
        await mkdir(dist, { recursive: true });
      else
        reject(x);
    }
  );
  const files = [
    ".editorconfig",
    "eslint.config.mjs",
    ["_gitignore", ".gitignore"],
    ".vscode",
    // 'scripts',
    "tsconfig.json",
    ".yarnrc.yml",
    ..._files
  ];
  await Promise.all(
    files.map(async (file) => {
      const [source, target] = "string" === typeof file ? [file, file] : file;
      await copy(join(src, source), join(dist, target));
    })
  );
};

const yarnAdd = async ({ names, devs, directory, opts = [] }) => {
  if (names?.length)
    await run(parseNi, ["-ED", ...opts, ...names], { cwd: directory });
  if (devs?.length)
    await run(parseNi, ["-ED", ...opts, ...devs], { cwd: directory });
};

export { ignoreEntryExist as a, copyFiles as c, ignoreNoEntry as i, mayReadPackageJson as m, writePackageJson as w, yarnAdd as y };
