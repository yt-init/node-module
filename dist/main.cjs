#!/usr/bin/env node
'use strict';

const node_url = require('node:url');
const node_path = require('node:path');
const node_child_process = require('node:child_process');
const commandLineArgs = require('command-line-args');
const lib = require('./shared/node-module.4d61a3ce.cjs');
require('node:fs/promises');
require('node:fs');
require('@antfu/ni');

var _documentCurrentScript = typeof document !== 'undefined' ? document.currentScript : null;
function _interopDefaultCompat (e) { return e && typeof e === 'object' && 'default' in e ? e.default : e; }

const commandLineArgs__default = /*#__PURE__*/_interopDefaultCompat(commandLineArgs);

const main = async () => {
  const options = commandLineArgs__default([{ name: "dir", defaultOption: true }]);
  const { dir } = options;
  if ("string" !== typeof dir || !dir)
    throw new Error("dir is required");
  const directory = node_path.resolve(process.cwd(), dir);
  console.log(`${directory} \u3092\u521D\u671F\u5316\u3057\u307E\u3059`);
  const __dirname = node_path.dirname(node_url.fileURLToPath((typeof document === 'undefined' ? require('u' + 'rl').pathToFileURL(__filename).href : (_documentCurrentScript && _documentCurrentScript.tagName.toUpperCase() === 'SCRIPT' && _documentCurrentScript.src || new URL('main.cjs', document.baseURI).href))));
  await lib.copyFiles({ dist: directory, src: node_path.resolve(__dirname, "..") });
  await node_child_process.execSync("volta pin node@lts yarn@4", { cwd: directory });
  const {
    volta,
    packageManager: pmOrig,
    ...packageJsonCont
  } = await lib.mayReadPackageJson(directory) || {};
  const packageManager = pmOrig || volta && "object" === typeof volta && "yarn" in volta && "string" === typeof volta.yarn && /^[0-9]+\.[0-9]+\.[0-9]+$/.test(volta.yarn) && `yarn@${volta.yarn}`;
  await lib.writePackageJson(directory, {
    name: node_path.basename(directory),
    version: "0.0.1",
    description: "",
    author: { name: "ytoune" },
    engines: { node: ">= 20" },
    ...volta ? { volta } : {},
    ...packageManager ? { packageManager } : {},
    license: "MIT",
    private: true,
    scripts: {
      test: 'eslint "**/*.{ts,tsx}" && tsc --noEmit && vitest run --passWithNoTests',
      "ts-node": "node -r esbuild-register"
    },
    dependencies: {},
    devDependencies: {},
    prettier: {
      useTabs: false,
      singleQuote: true,
      trailingComma: "all",
      semi: false,
      arrowParens: "avoid",
      htmlWhitespaceSensitivity: "ignore"
    },
    ...packageJsonCont || {}
  });
  await lib.yarnAdd({
    directory,
    devs: [
      "@types/node",
      "@typescript-eslint/eslint-plugin",
      "@typescript-eslint/parser",
      "esbuild",
      "esbuild-register",
      "eslint",
      "eslint-config-prettier",
      "eslint-plugin-import",
      "eslint-plugin-github",
      "eslint-import-resolver-typescript",
      "prettier",
      "rimraf",
      "typescript",
      "vite",
      "vitest",
      "@eslint/compat",
      "globals",
      "@eslint/js",
      "@eslint/eslintrc"
    ]
  });
};
main().then(() => {
  console.log("Done!!");
}).catch((x) => {
  console.error(x);
  process.exit(1);
});
