'use strict';

const lib = require('./shared/node-module.4d61a3ce.cjs');
require('node:fs/promises');
require('node:path');
require('node:fs');
require('@antfu/ni');



exports.copyFiles = lib.copyFiles;
exports.ignoreEntryExist = lib.ignoreEntryExist;
exports.ignoreNoEntry = lib.ignoreNoEntry;
exports.yarnAdd = lib.yarnAdd;
